import java.util.Scanner;

/**
 * Created by karab on 17.02.2017.
 * метод.split
 */
public class Demo {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String[] array = new String[3];
        Rational racional1 = new Rational();
        Rational racional2 = new Rational();
        Rational racional3 = new Rational();
        String stringRacional;
        int i = 0;

        System.out.println("Введите выражение (a/b ? c/d) \n Где a,b,c,d- числа, а ?- знак (+,-,*,/) ");
        stringRacional = in.nextLine();
        for (String minstring : stringRacional.split(" ")) {
            array[i] = minstring;
            i++;
        }

        racional1.racionalSplit(array[0]);
        racional2.racionalSplit(array[2]);

        switch (array[1]) {
            case "+":
                racional3 = racional1.summ(racional2);
                System.out.println(stringRacional+racional3.reduction());
                break;
            case "-":
                racional3 = racional1.subtraction(racional2);
                System.out.println(stringRacional+racional3.reduction());
                break;
            case "*":
                racional3 = racional1.multiplication(racional2);
                System.out.println(stringRacional+racional3.reduction());
                break;
            case "/":
                racional3 = racional1.division(racional2);
                System.out.println(stringRacional+racional3.reduction());
                break;
            default:
                System.out.println("Неправильно введены данные");
                break;
        }
    }
}